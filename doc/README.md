Mopsa Manual
============

These are the sources of the [Mopsa](mopsa.lip6.fr) Manual.


Requirements
------------

```bash
$ pip install sphinx sphinx_rtd_theme
```

Build
-----
```bash
$ make html
$ make latexpdf
```

.. _cpy-benchs:

Multilanguage Benchmarks (Python+C)
===================================

We analyzed a few different libraries available on Github. Here are the corresponding benchmark repositories:

- `levenshtein <https://gitlab.com/mopsa/benchmarks/cpython-benchmarks/levenshtein-analysis>`_,

- `pyahocorasick <https://gitlab.com/mopsa/benchmarks/cpython-benchmarks/pyahocorasick-analysis>`_,

- `cdistance <https://gitlab.com/mopsa/benchmarks/cpython-benchmarks/cdistance-analysis>`_,

- `bitarray <https://gitlab.com/mopsa/benchmarks/cpython-benchmarks/bitarray-analysis>`_,

- `noise <https://gitlab.com/mopsa/benchmarks/cpython-benchmarks/noise-analysis>`_,

- `llist <https://gitlab.com/mopsa/benchmarks/cpython-benchmarks/llist-analysis>`_.
